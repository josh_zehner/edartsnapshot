﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace EDartSnapshot
{
    public partial class EdartSnapshotService : ServiceBase
    {
        private static EDartSnapshot eDartSnapshot;
        private Thread thread;
        private IContainer container;

        public EdartSnapshotService()
        {
            InitializeComponent();
            if (!EventLog.SourceExists("EDartSnapshot"))
            {
                EventLog.CreateEventSource("EDartSnapshot", "");
            }
            eventLog.Source = "EDartSnapshot";
            eventLog.Log = "";

        }

        public void StartService(string[] args)
        {
            OnStart(args);
        }


        public void StopService()
        {
            OnStop();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                thread = new Thread(StartEDartThread);
                thread.SetApartmentState(ApartmentState.STA);
                thread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
                thread.Start();

                eventLog.WriteEntry("EDartSnapshot has started");
            }
            catch (Exception e)
            {
                eventLog.WriteEntry("Failed to initialize EDartSnapshot", EventLogEntryType.Error);
                eventLog.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (eDartSnapshot != null)
                {
                    eDartSnapshot.CleanUp();
                    Application.Exit();
                    if (!(thread == null || thread.ThreadState != System.Threading.ThreadState.Running))
                    {
                        thread.Abort();
                    }
                }
                eventLog.WriteEntry("EDartSnapshot has stopped");
            }
            catch (Exception e)
            {
                eventLog.WriteEntry("Failed to clean up EDartSnapshot", EventLogEntryType.Error);
                eventLog.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }
            
        }

        private void StartEDartThread(object obj)
        {
            eDartSnapshot = new EDartSnapshot();
            Application.Run();
        }

        public DartInterface GetDartInterface()
        {
            return eDartSnapshot.GetDartInterface();
        }

    }
}
