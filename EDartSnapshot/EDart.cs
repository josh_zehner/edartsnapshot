﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using AxEDARTINTERFACELib;

namespace EDartSnapshot
{
    public class EDart
    {
        private static TraceSource _ts = new TraceSource("EDartSnapshotTraceSource"); 

        public EDart()
        {
            DataPoints = new List<DataPoint>();
        }

        public string Name { get; set; }
        public string IP { get; set; }
        public string Tonnage { get; set; }
        public int ConnectionId { get; set; }
        public EDartStatus Status { get; set; }

        public List<DataPoint> DataPoints { get; set; }
        

        public void ResetDataPoints()
        {
            DataPoints.Clear();
        }

        public void AddDataPoint(object data, string toolName, double standard, DataType type, string status = "", int cycles = 0)
        {
            if (type == DataType.Cycle && !standard.Equals(0.0))
            {
                var time = Convert.ToDouble(data);
                if (standard*Convert.ToDouble(ConfigurationManager.AppSettings["CycleThreshold"]) < time)
                {
                    _ts.TraceEvent(TraceEventType.Warning, 50, string.Format("Cycle time was above a threshold on {0}, will discard {1}", Name, time));
                    type = DataType.Downtime;
                    status = "Above Threshold";
                }
            }
            var dataPoint = new DataPoint
            {
                Data = data,
                ToolName = toolName,
                Standard = standard,
                Type = type,
                Status = status,
                Cycles = cycles
            };

            DataPoints.Add(dataPoint);
        }

        public override string ToString()
        {
            return string.Format("ConnectionId: {0}, Status: {1}, MachineName: {2}, Tonnage: {3}, IP: {4}, DataPoints: {5}", ConnectionId, Status, Name,  Tonnage, IP, DataPoints.Count);
        }

        public void SwitchToDowntime(List<DataPoint> dataPoints)
        {
            foreach (var dataPoint in dataPoints)
            {
                dataPoint.Type = DataType.Downtime;
                dataPoint.Status = "Startup Time";
            }

            //DataPoints.AddRange(dataPoints);
        }
    }

    public class DataPoint
    {
        private string _toolName;

        public DataPoint()
        {
            RecordDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Timezone"]));
        }

        public DataPoint(DateTime recordTime)
        {
            RecordDateTime = recordTime;
        }

        public DateTime RecordDateTime { get; private set; }
        public string ToolName { get { return _toolName; } set { _toolName = value.Split('/')[0]; } }
        public object Data { get; set; }
        public double Standard { get; set; }
        public DataType Type { get; set; }
        public string Status { get; set; }
        public int Cycles { get; set; }


    }

    
}
