﻿using System;
using System.Timers;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using OfficeOpenXml;


namespace EDartSnapshot
{
    public class EDartSnapshot
    {
        
        private static DartInterface _dartInterface;
        private Timer _timer;
        private int _firstShiftEndTime = -1;
        private int _secondShiftEndTime = -1;
        private int _thirdShiftEndTime = -1;
        private TraceSource _ts = new TraceSource("EDartSnapshotTraceSource");

        public EDartSnapshot()
        {
            _dartInterface = new DartInterface();
            _timer = new Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
            _timer.AutoReset = true;
            _timer.Elapsed += TimerOnElapsed;
            _timer.Start();

            
            var parsed = Int32.TryParse(ConfigurationManager.AppSettings["FirstShiftEndTime"], out _firstShiftEndTime);
            if (!parsed) { //TODO: print failed 
            }
            parsed = Int32.TryParse(ConfigurationManager.AppSettings["SecondShiftEndTime"], out _secondShiftEndTime);
            if (!parsed)
            { //TODO: print failed 
            }
            parsed = Int32.TryParse(ConfigurationManager.AppSettings["thirdShiftEndTime"], out _thirdShiftEndTime);
            if (!parsed)
            { //TODO: print failed 
            }

        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            try { 
                _dartInterface.Record();
                var storagePath = ConfigurationManager.AppSettings["ServerStoragePath"];

                var now = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Timezone"]));

                var shift = Shift.Unknown;
                if (now.Hour == _firstShiftEndTime && now.Minute == 0)
                {
                    shift = Shift.First;
                }
                else if (now.Hour == _secondShiftEndTime && now.Minute == 0)
                {
                    shift = Shift.Second;
                }
                else if (now.Hour == _thirdShiftEndTime && now.Minute == 0)
                {
                    shift = Shift.Third;
                }

                if (shift != Shift.Unknown)
                {
                    var path = string.Format("{0}CycleReport-{1}-{2}Shift.xlsx", storagePath, now.ToString("yy-MM-dd"), shift);
                    var htmlSummary = ExcelService.CreateExcel(path, _dartInterface.GetEdartList(), shift);
                    EmailService.SendEmail(path, shift, htmlSummary);
                    _dartInterface.ClearData();
                }
            }
            catch (Exception ex)
            { 
                _ts.TraceEvent(TraceEventType.Error, 20, ex.ToString());
            }
        }

        public bool CleanUp()
        {
            _timer.Stop();
            _timer.Dispose();
            _dartInterface.CloseConnection();
            return true;
        }

        internal DartInterface GetDartInterface()
        {
            return _dartInterface;
        }

        
    }
}
