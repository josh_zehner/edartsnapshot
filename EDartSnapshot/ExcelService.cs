﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Windows.Forms.DataVisualization.Charting;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;

namespace EDartSnapshot
{
    public static class ExcelService
    {
        private static List<EDart> _eDarts;
        private static TraceSource _ts = new TraceSource("EDartSnapshotTraceSource");
        private static string _htmlSummary = "";
        private static Chart _chart;
        private static int _offStatusCount = 0;

        public static string CreateExcel(string path, List<EDart> edartList, Shift shift)
        {
            try
            {
                _htmlSummary = "";
                _eDarts = edartList;
                ExcelPackage pck = new ExcelPackage(new FileInfo(path));
                AddCycleReportTab(ref pck);
                AddDowntimeTab(ref pck);
                AddHistoryTrendChart(ref pck, shift);
                AddDataTab(ref pck);
                pck.Save();
                return _htmlSummary;
            }
            catch (Exception ex)
            {
                _ts.TraceEvent(TraceEventType.Error, 20, ex.ToString());
            }
            return null;
        }

        public static MemoryStream ChartStream()
        {
            var imageStream = new MemoryStream();
            _chart.SaveImage(imageStream, ChartImageFormat.Gif);
            return imageStream;
        }

        private static void AddDowntimeTab(ref ExcelPackage pck)
        {
            var ws = pck.Workbook.Worksheets.Add("Downtime");
            ws.Cells["A1"].Value = "Total Uptime";
            ws.Cells["A1"].Style.Font.Bold = true;
            ws.Cells["A4"].Value = "Machine";
            ws.Cells["B4"].Value = "Downtime";
            ws.Cells["C4"].Value = "Uptime";
            ws.Cells["A4:C4"].Style.Font.Bold = true;

            _htmlSummary += ConfigurationManager.AppSettings["DowntimeHtmlHeader"];

            var index = 5;
            var totalCount = 0;
            var totalDown = 0;
            for (int i = 0; i < _eDarts.Count; i++)
            {
                var eDart = _eDarts[i];
                var downDataPoints = eDart.DataPoints.Where(s => s.Type.Equals(DataType.Downtime)).ToList();

                var uptime = (double)(eDart.DataPoints.Count - downDataPoints.Count)/eDart.DataPoints.Count;

                var name = eDart.Name;
                if (string.IsNullOrEmpty(name))
                {
                    name = eDart.IP;
                }

                ws.Cells["A" + index].Value = name;
                ws.Cells["B" + index].Value = downDataPoints.Count;
                ws.Cells["C" + index].Value = uptime;
                ws.Cells["C" + index].Style.Numberformat.Format = "0.00%";

                totalDown = totalDown + downDataPoints.Count;
                totalCount = totalCount + eDart.DataPoints.Count;

                var html = ConfigurationManager.AppSettings["DowntimeHtmlRow"];

                html = html.Replace("@@NAME@@", name);
                html = html.Replace("@@DOWNTIME@@", downDataPoints.Count.ToString());
                html = html.Replace("@@UPTIME@@", string.Format("{0:n2}",(uptime*100)));

                _htmlSummary += html;

                index++;
            }

            
            ws.Cells["A2"].Value = (double)(totalCount - totalDown)/totalCount;
            ws.Cells["A2"].Style.Numberformat.Format = "0.00%";

            ws.Cells[ws.Dimension.Address].AutoFitColumns();
            _htmlSummary = _htmlSummary.Replace("@@TOTALUPTIME@@", string.Format("{0:n2}",(double)(totalCount - totalDown) / totalCount * 100));
            _htmlSummary += ConfigurationManager.AppSettings["HtmlFooter"];
        }

        private static void AddCycleReportTab(ref ExcelPackage pck)
        {
            var ws = pck.Workbook.Worksheets.Add("Cycles");
            ws.Cells["A1"].Value = "Machine";
            ws.Cells["B1"].Value = "Tool";
            ws.Cells["C1"].Value = "Tonnage";
            ws.Cells["D1"].Value = "Average";
            ws.Cells["E1"].Value = "Standard";
            ws.Cells["F1"].Value = "Sample Count";
            ws.Cells["G1"].Value = "Cycle Count";
            ws.Cells["A1:E1"].Style.Font.Bold = true;

            _htmlSummary += ConfigurationManager.AppSettings["CycleHtmlHeader"];

            var rowList = new List<Tuple<string[], double[], int, int>>();
            foreach (var eDart in _eDarts)
            {
                var uniqueTools = eDart.DataPoints.Select(s => s.ToolName).Distinct().ToList();
                foreach (var uniqueTool in uniqueTools)
                {
                    var uniqueToolString = uniqueTool;
                    var reportDP = eDart.DataPoints.Where(s => s.Type.Equals(DataType.Cycle) && s.ToolName.Equals(uniqueToolString)).ToList();
                    if (reportDP.Any())
                    {
                        var orderedList = reportDP.OrderBy(s => s.RecordDateTime).ToList();
                        
                        if (orderedList.Count > 10)
                        {
                            eDart.SwitchToDowntime(orderedList.GetRange(0, 10));
                            orderedList = orderedList.GetRange(9, orderedList.Count() - 10);
                        }

                        var average = orderedList.Average(s => Convert.ToDouble(s.Data));
                        var standard = orderedList.Last().Standard;
                        var lowCycleCount = orderedList.Min(s => s.Cycles);
                        var cycleCount = orderedList.Max(s => s.Cycles) - lowCycleCount;
                        var diff = average - standard;
                        var name = eDart.Name;
                        if (string.IsNullOrEmpty(name)) {  name = eDart.IP; }
                        var row = Tuple.Create(new []{ name, uniqueToolString, eDart.Tonnage }, new []{ average, standard, diff }, orderedList.Count(), cycleCount);
                        rowList.Add(row);
                    }
                }
            }

            var orderedRows = rowList.OrderByDescending(s => s.Item3).ThenByDescending(s => s.Item2[2]);

            

            var greenList = orderedRows.Where(s => s.Item2[2] <= 0.05);
            var yellowList = orderedRows.Where(s => s.Item2[2] > 0.05 && s.Item2[2] < 0.5 && s.Item3 > 25);
            var redList = orderedRows.Where(s => s.Item2[2] >= 0.5 && s.Item3 > 25);
            var orangeList = orderedRows.Where(s => s.Item2[2] > 0.05 && s.Item3 <= 25);

            _offStatusCount = yellowList.Count() + redList.Count();

            var sortedList = redList.Concat(yellowList).Concat(greenList).Concat(orangeList);

            var index = 2;
            foreach (var tuple in sortedList)
            {
                var color = Color.LightGreen;
                var hexColor = "#76FC51";
                if (tuple.Item2[2] > 0.05 && tuple.Item2[2] < 0.5)
                {
                    color = Color.Yellow;
                    hexColor = "#FCFA51";
                }
                else if (tuple.Item2[2] >= 0.5)
                {
                    color = Color.Red;
                    hexColor = "#F24B4B";
                }

                if (color != Color.LightGreen && tuple.Item3 <= 25)
                {
                    color = Color.Orange;
                    hexColor = "#FFA500";
                }

                ws.Cells["A" + index].Value = tuple.Item1[0];
                ws.Cells["B" + index].Value = tuple.Item1[1];
                ws.Cells["C" + index].Value = tuple.Item1[2];
                ws.Cells["D" + index].Value = tuple.Item2[0];
                ws.Cells["E" + index].Value = tuple.Item2[1];
                ws.Cells["F" + index].Value = tuple.Item3;
                ws.Cells["G" + index].Value = tuple.Item4;
                ws.Cells["A" + index + ":G" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A" + index + ":G" + index].Style.Fill.BackgroundColor.SetColor(color);

                var html = ConfigurationManager.AppSettings["CycleHtmlRow"];

                html = html.Replace("@@Color@@", hexColor);
                html = html.Replace("@@Name@@", tuple.Item1[0]);
                html = html.Replace("@@Tool@@", tuple.Item1[1]);
                html = html.Replace("@@Average@@", string.Format("{0:n2}", tuple.Item2[0]));
                html = html.Replace("@@Standard@@", string.Format("{0:n2}", tuple.Item2[1]));
                html = html.Replace("@@SampleCount@@", tuple.Item3.ToString());
                html = html.Replace("@@CycleCount@@", tuple.Item4.ToString());

                _htmlSummary += html;

                index++;
            }

            

            ws.Cells[ws.Dimension.Address].AutoFitColumns();

            _htmlSummary += ConfigurationManager.AppSettings["HtmlFooter"];
        }

        private static void AddDataTab(ref ExcelPackage pck)
        {
            var ws = pck.Workbook.Worksheets.Add("Data");
            ws.Cells["A1"].Value = "Date";
            ws.Cells["B1"].Value = "Time";
            ws.Cells["C1"].Value = "Machine";
            ws.Cells["D1"].Value = "Tool";
            ws.Cells["E1"].Value = "Tonnage";
            ws.Cells["F1"].Value = "Standard";
            ws.Cells["G1"].Value = "Actual";
            ws.Cells["H1"].Value = "Cycle Count";
            ws.Cells["I1"].Value = "Down Cause";
            ws.Cells["A1:I1"].Style.Font.Bold = true;

            var index = 2;
            for(int i = 0; i < _eDarts.Count; i++)
            {
                var eDart = _eDarts[i];
                var cycleDataPoints = eDart.DataPoints.Where(s => s.Type.Equals(DataType.Cycle));
                foreach (var dataPoint in cycleDataPoints)
                {
                    ws.Cells["A" + index].Value = dataPoint.RecordDateTime.ToShortDateString();
                    ws.Cells["B" + index].Value = dataPoint.RecordDateTime.ToShortTimeString();
                    ws.Cells["C" + index].Value = eDart.Name;
                    ws.Cells["D" + index].Value = dataPoint.ToolName;
                    ws.Cells["E" + index].Value = eDart.Tonnage;
                    ws.Cells["F" + index].Value = dataPoint.Standard;
                    ws.Cells["G" + index].Value = Convert.ToDouble(dataPoint.Data);
                    ws.Cells["H" + index].Value = dataPoint.Cycles;
                    index++;
                }

                var downtimeCycleDataPoints = eDart.DataPoints.Where(s => s.Type.Equals(DataType.Downtime));
                foreach (var dataPoint in downtimeCycleDataPoints)
                {
                    ws.Cells["A" + index].Value = dataPoint.RecordDateTime.ToShortDateString();
                    ws.Cells["B" + index].Value = dataPoint.RecordDateTime.ToShortTimeString();
                    ws.Cells["C" + index].Value = eDart.Name;
                    ws.Cells["D" + index].Value = dataPoint.ToolName;
                    ws.Cells["E" + index].Value = eDart.Tonnage;
                    ws.Cells["F" + index].Value = dataPoint.Standard;
                    ws.Cells["G" + index].Value = dataPoint.Data;
                    ws.Cells["I" + index].Value = dataPoint.Status;
                    index++;
                }

                var unknownCycleDataPoints = eDart.DataPoints.Where(s => s.Type.Equals(DataType.Unknown));
                foreach (var dataPoint in unknownCycleDataPoints)
                {
                    ws.Cells["A" + index].Value = dataPoint.RecordDateTime.ToShortDateString();
                    ws.Cells["B" + index].Value = dataPoint.RecordDateTime.ToShortTimeString();
                    ws.Cells["C" + index].Value = eDart.Name;
                    ws.Cells["D" + index].Value = dataPoint.ToolName;
                    ws.Cells["E" + index].Value = eDart.Tonnage;
                    ws.Cells["F" + index].Value = dataPoint.Standard;
                    ws.Cells["G" + index].Value = "Unknown";
                    ws.Cells["I" + index].Value = dataPoint.Status;
                    index++;
                }

            }

            ws.Cells[ws.Dimension.Address].AutoFitColumns();
        }

        public static void AddHistoryTrendChart(ref ExcelPackage pck, Shift shift)
        {
            //excel
            //var dataPoints = new Dictionary<string, int>();
            var dataPoints = new List<string[]>();
            var xData = new List<string>();
            var yData = new List<int>();

            var ws = pck.Workbook.Worksheets.Add("Trends");

            if (File.Exists("trenddata.txt"))
            {
                var entries = File.ReadAllLines("trenddata.txt");
                dataPoints.AddRange(entries.Select(entry => entry.Split(',')));
            }

            if (!dataPoints.Any() || dataPoints[0][0] == null ||
                !dataPoints[0][0].Equals(
                    TimeZoneInfo.ConvertTime(DateTime.Now,
                        TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Timezone"]))
                        .ToShortDateString()))
            {
                var data = new string[4];


                data[0] = TimeZoneInfo.ConvertTime(DateTime.Now,
                    TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Timezone"]))
                    .ToShortDateString();

                data[(int) shift + 1] = _offStatusCount.ToString();

                dataPoints.Insert(0, data);
            }
            else
            {
                dataPoints[0][(int) shift + 1] = _offStatusCount.ToString();
            }

            for (var i = 0; i < dataPoints.Count && i < 30; i++)
            {
                var convert = 0;
                Int32.TryParse(dataPoints[i][(int) shift + 1], out convert);
                xData.Add(dataPoints[i][0]);
                yData.Add(convert);
            }

            xData.Reverse();
            yData.Reverse();

            for (var i = 1; i <= xData.Count && i <= 30; i++)
            {
                ws.Cells["S" + i].Value = xData[i-1];
                ws.Cells["T" + i].Value = yData[i-1];
            }

            var chart = ws.Drawings.AddChart("Trend", eChartType.Line);
            chart.Legend.Position = eLegendPosition.Bottom;
            chart.Legend.Add();
            chart.Title.Text = string.Format("Trends for {0} shift {1}-{2}", shift, xData.First(), xData.Last());
            chart.SetPosition(0,0);
            chart.SetSize(900,500);
            chart.Series.Add(ws.Cells["T1:T30"], ws.Cells["S1:S30"]);
            chart.Style = eChartStyle.Style2;

            //image for email
            _chart = new Chart();
            _chart.Size = new Size(600,250);

            var chartArea = new ChartArea();
            chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            _chart.ChartAreas.Add(chartArea);

            var series = new Series();
            series.Name = "Trend";
            series.ChartType = SeriesChartType.FastLine;
            series.XValueType = ChartValueType.String;
            _chart.Series.Add(series);

            
            _chart.Series["Trend"].Points.DataBindXY(xData, yData);

            _chart.Invalidate();

            _htmlSummary += ConfigurationManager.AppSettings["TrendHTML"];

            using (var file = new StreamWriter("trenddata.txt"))
            {
                foreach (var dataPoint in dataPoints)
                {
                    file.WriteLine(string.Join(",",dataPoint));
                }
            }
            

        }
    }
}
