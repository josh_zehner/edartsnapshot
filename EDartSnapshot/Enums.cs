﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDartSnapshot
{
    public enum DataType
    {
        Downtime,
        Cycle,
        Unknown
    }

    public enum Shift
    {
        First = 0,
        Second = 1,
        Third = 2,
        Unknown = 3
    }
}
