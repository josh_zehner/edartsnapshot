﻿using System;
using System.Configuration;
using System.ServiceProcess;
using System.IO;
using System.Web.UI;

namespace EDartSnapshot
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        
        
        static void Main(string[] args)
        {
            EdartSnapshotService service = new EdartSnapshotService();
            if (Environment.UserInteractive)
            {
                service.StartService(args);
                while (true)
                {
                    Console.WriteLine("Enter Command or 'Exit' to stop");
                    var cmd = Console.ReadLine();
                    if (!string.IsNullOrEmpty(cmd))
                    {
                        if (cmd.ToLower().Contains("count"))
                        {
                            if (cmd.ToLower().Contains("edart"))
                            {
                                Console.WriteLine("There are " + service.GetDartInterface().GetEdartCount() + " edarts recorded");
                            }
                            else if (cmd.ToLower().Contains("data"))
                            {
                                Console.WriteLine("There are " + service.GetDartInterface().GetDataCount() + " data points recorded");
                            }
                        }
                        else if (cmd.ToLower().Contains("add new"))
                        {
                            Console.WriteLine("Enter in the address:");
                            var address = Console.ReadLine();
                            service.GetDartInterface().AddConnection(address);
                            
                        }
                        else if (cmd.ToLower().Contains("excel"))
                        {
                            var fileName = ConfigurationManager.AppSettings["ServerStoragePath"] + "Test.xlsx";

                            if (File.Exists(fileName))
                            {
                                File.Delete(fileName);
                            }
                            var htmlSummary = ExcelService.CreateExcel(fileName, service.GetDartInterface().GetEdartList(), Shift.First);

                        }
                        else if (cmd.ToLower().Contains("mail"))
                        {
                            var fileName = ConfigurationManager.AppSettings["ServerStoragePath"] + "Test.xlsx";

                            if (File.Exists(fileName))
                            {
                                File.Delete(fileName);
                            }
                            var htmlSummary = ExcelService.CreateExcel(fileName, service.GetDartInterface().GetEdartList(), Shift.First);

                            EmailService.SendEmail(fileName, Shift.First, htmlSummary);
                        }
                        else if (cmd.ToLower().Contains("dump"))
                        {
                            service.GetDartInterface().DumpInfo();
                        }
                        else if (cmd.ToLower().Contains("exit"))
                        {
                            service.StopService();
                            break;
                        }
                    }

                }
            }
            else
            {
                ServiceBase.Run(service);
            }
        }
    }
}
