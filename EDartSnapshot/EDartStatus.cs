﻿namespace EDartSnapshot
{
    public enum EDartStatus
    {
        Unknown = 0,
        Up = 1,
        Down = 2
    }
}
