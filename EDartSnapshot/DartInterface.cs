﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using AxEDARTINTERFACELib;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;

namespace EDartSnapshot
{
    public class DartInterface
    {

        private AxEDartInterface _eDartInterface;
        private Dictionary<int, EDart> _eDarts;
        private int _userId = 35111;
        private TraceSource _ts = new TraceSource("EDartSnapshotTraceSource");
        private int MachineNameIndex;
        private int TonnageIndex;
        private int ToolNameIndex;

        public DartInterface()
        {
            _ts.TraceEvent(TraceEventType.Information, 100, "DartInterface has started");
            _eDarts = new Dictionary<int, EDart>();

            _eDartInterface = new AxEDartInterface();
            _eDartInterface.CreateControl();
            

            _eDartInterface.ConnectionMade += eDartInterface_ConnectionMade;
            _eDartInterface.ConnectionDropped += eDartInterface_ConnectionDropped;
            _eDartInterface.DataReceived += eDartInterface_DataReceived;
            _eDartInterface.EDartCycled += eDartInterface_EDartCycled;
            //_eDartInterface.EDartFound += eDartInterface_EDartFound;
            _eDartInterface.QueryReturned += eDartInterface_QueryReturned;
            
            var checkNetworkStatus = _eDartInterface.CheckLocalNet();
            if (checkNetworkStatus == 0)
            {
                _ts.TraceEvent(TraceEventType.Warning, 400, "CheckLocalNet returned 0.  Socket system was not initialized.  EDartSnapshot will not log any data");
            }
            
            PopulateEdarts();

        }

        #region event methods

        void eDartInterface_EDartFound(object sender, _DEDartInterfaceEvents_EDartFoundEvent deEvent)
        {
            _ts.TraceEvent(TraceEventType.Information, 101, "Found new EDart at index "+deEvent.computer_index);
            var eDart = PopulateNewEDart("", deEvent.computer_index);
            _eDarts.Add(eDart.ConnectionId, eDart);
        }

        void eDartInterface_EDartCycled(object sender, _DEDartInterfaceEvents_EDartCycledEvent deEvent)
        {

        }

        void eDartInterface_ConnectionMade(object sender, _DEDartInterfaceEvents_ConnectionMadeEvent deEvent)
        {
            _ts.TraceEvent(TraceEventType.Information, 104, "Connection made to " + deEvent.who);
            if (deEvent.user_data == _userId)
            {
                if (_eDarts.ContainsKey(deEvent.who))
                {
                    _eDarts[deEvent.who].Status = EDartStatus.Up;
                }
            }
        }

        void eDartInterface_ConnectionDropped(object sender, _DEDartInterfaceEvents_ConnectionDroppedEvent deEvent)
        {
            _ts.TraceEvent(TraceEventType.Warning, 104, "Connection dropped to " + deEvent.who);
            if (deEvent.user_data == _userId)
            {
                if (_eDarts.ContainsKey(deEvent.who))
                {
                    _eDarts[deEvent.who].Status = EDartStatus.Down;
                }
            }
            AttemptReconnect(_eDarts[deEvent.who].IP);
        }

        void eDartInterface_QueryReturned(object sender, _DEDartInterfaceEvents_QueryReturnedEvent deEvent)
        {
            _ts.TraceEvent(TraceEventType.Information, 200, deEvent.who + ": " + deEvent.asked);
            _ts.TraceEvent(TraceEventType.Information, 201, deEvent.who + ": " + deEvent.query_string);
        }

        void eDartInterface_DataReceived(object sender, _DEDartInterfaceEvents_DataReceivedEvent deEvent)
        {
            var dataName = _eDartInterface.GetDataName(deEvent.data_index);

            if (GetAppSetting("MachineName").Equals(dataName))
            {
                _eDarts[deEvent.who].Name = deEvent.data.ToString();
            }
            else if (GetAppSetting("TonnageName").Equals(dataName))
            {
                _eDarts[deEvent.who].Tonnage = deEvent.data.ToString();
            }
            
            _ts.TraceEvent(TraceEventType.Information, 205, string.Format("deEvent - Data: {0}, Index: {1}, Status: {2}, user_data: {3}, Who: {4}, DataName: {5}", deEvent.data, deEvent.data_index, deEvent.status, deEvent.user_data, deEvent.who, _eDartInterface.GetDataName(deEvent.data_index)));
        }
        #endregion

        #region Close up shop

        public void CloseConnection()
        {
            _eDartInterface.ConnectionMade -= eDartInterface_ConnectionMade;
            _eDartInterface.ConnectionDropped -= eDartInterface_ConnectionDropped;
            _eDartInterface.DataReceived -= eDartInterface_DataReceived;
            _eDartInterface.EDartCycled -= eDartInterface_EDartCycled;
            _eDartInterface.EDartFound -= eDartInterface_EDartFound;
            _eDartInterface.QueryReturned -= eDartInterface_QueryReturned;

            foreach (var eDart in _eDarts.Values)
            {
                _eDartInterface.RemoveConnection(eDart.IP);
            }
        }

        #endregion

        #region private methods

        private EDart PopulateNewEDart(string address, int connectionId)
        {
            if (string.IsNullOrEmpty(address)) { address = _eDartInterface.LookupIP(connectionId); }
            
            var eDart = new EDart
            {
                IP = address,
                ConnectionId = connectionId,
                Status = EDartStatus.Unknown
            };

            return eDart;
        }

        private void PopulateEdarts()
        {
            var baseIp = GetAppSetting("BaseEdartIp");
            var edartIps = GetAppSetting("EdartIp").Split(';').Select(i => i.Trim()).ToList();
            foreach (var edartIp in edartIps)
            {
                AddConnection(baseIp + "." + edartIp);
            }

            _eDartInterface.SynchronizeData();
        }

        private void PopulateData(int who)
        {
            _eDartInterface.OpenDataChannel(GetAppSetting("TonnageName"), true, who);
            _eDartInterface.OpenDataChannel(GetAppSetting("MachineName"), true, who);
            _eDartInterface.OpenDataChannel(GetAppSetting("StandardName"), true, who);
            _eDartInterface.OpenDataChannel(GetAppSetting("ToolName"), false, who);
            _eDartInterface.OpenDataChannel(GetAppSetting("CycleName"), false, who);
            _eDartInterface.OpenDataChannel(GetAppSetting("StatusName"), false, who);
            _eDartInterface.OpenDataChannel(GetAppSetting("CycleCountName"), false, who);

        }

        private string GetAppSetting(string name)
        {
            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[name]))
            {
                return ConfigurationManager.AppSettings[name];
            }
            _ts.TraceEvent(TraceEventType.Warning, 901, "Could not find app setting for '" + name + "'");
            return "";
        }

        private void AttemptReconnect(string ip)
        {
            Timer t = new Timer();
            t.Interval = 10000.0;
            t.Elapsed += (ElapsedEventHandler)((s, e) =>
            {
                _eDartInterface.AddConnection(ip, _userId);
                t.Stop();
            });
            t.Start();
        }

        private void ResetConnections()
        {
            foreach (var eDart in _eDarts)
            {
                _eDartInterface.RemoveConnection(eDart.Value.IP);
            }
            _eDarts.Clear();
            PopulateEdarts();
        }

        #endregion

        internal void Record()
        {
            foreach (var eDart in _eDarts)
            {
                try {
                    if (eDart.Value.Status != EDartStatus.Up) {
                        _eDartInterface.AddConnection(eDart.Value.IP, _userId);
                        _ts.TraceEvent(TraceEventType.Warning, 211, eDart.Value.IP + " is not connected. Attempting reconnect.");
                    }

                    var status = _eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("StatusName")), eDart.Key);
                    if (status.Equals("Production"))
                    {
                        eDart.Value.AddDataPoint(
                            _eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("CycleName")), eDart.Key),
                            _eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("ToolName")), eDart.Key).ToString(),
                            Convert.ToDouble(_eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("StandardName")), eDart.Key)),
                            DataType.Cycle,
                            cycles:(int)_eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("CycleCountName")), eDart.Key));
                        eDart.Value.Status = EDartStatus.Up;
                    }
                    else if(status.Equals("No Job")
                            || status.Equals("Down")
                            || status.Equals("Setup")
                            || status.Equals("Pre-Approval")
                            || status.Equals("QC Approval"))
                    {
                        _ts.TraceEvent(TraceEventType.Information, 301, eDart.Value.IP + " returned status '" + status + "'");
                        eDart.Value.AddDataPoint("", (string)_eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("ToolName")), eDart.Key), 0, DataType.Downtime, status.ToString());
                    }
                    else
                    {
                        _ts.TraceEvent(TraceEventType.Warning, 212, eDart.Value.IP + " returned unknown status.  Most likely down");
                        eDart.Value.AddDataPoint("", (string)_eDartInterface.GetData(_eDartInterface.GetDataIndex(GetAppSetting("ToolName")), eDart.Key), 0, DataType.Unknown, status.ToString());
                        eDart.Value.Status = EDartStatus.Unknown;
                        AttemptReconnect(eDart.Value.IP);
                    }
                    
                }
                catch (Exception ex)
                { 
                    _ts.TraceEvent(TraceEventType.Warning, 20, ex.ToString());
                    _ts.TraceEvent(TraceEventType.Error, 210, eDart.Value.IP + " failed to record data");
                    eDart.Value.AddDataPoint("", "Unknown", 0, DataType.Downtime, "Exception");
                }
            }
        }

        internal void ClearData()
        {
            foreach (var eDart in _eDarts)
            {
                eDart.Value.ResetDataPoints();
            }

            ResetConnections();
        }

        internal int GetEdartCount()
        {
            return _eDarts.Count;
        }

        internal int GetDataCount()
        {
            var count = 0;
            foreach (var eDart in _eDarts)
            {
                count =+ eDart.Value.DataPoints.Count;
            }

            return count;
        }

        public int GetTotalResponses()
        {
            return _eDartInterface.TotalResponses();
        }

        public void AddConnection(string address)
        {
            if (String.IsNullOrWhiteSpace(address)) { return; }

            var index = _eDartInterface.AddConnection(address, _userId);
            PopulateData(index);
            _eDarts.Add(index, PopulateNewEDart(address, index));
            
            return;
        }

        public void DumpInfo()
        {
            foreach (var eDart in _eDarts)
            {
                _ts.TraceEvent(TraceEventType.Information, 998, eDart.ToString());   
            }
        }


        internal List<EDart> GetEdartList()
        {
            return _eDarts.Values.ToList();
        }
    }
}
