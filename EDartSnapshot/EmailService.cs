﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Text;
using System.Web;

namespace EDartSnapshot
{
    public static class EmailService
    {
        private const string ReportDateKey = "@@REPORTDATE@@";
        private const string ShiftKey = "@@SHIFT@@";
        private const string ReportSummaryKey = "@@REPORTSUMMARY@@";

        private static TraceSource _ts = new TraceSource("EDartSnapshotTraceSource");

        public static bool SendEmail(string reportPath, Shift shift, string reportSummary)
        {
            try
            {
                
                var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]);
                smtpClient.Port = Int32.Parse(ConfigurationManager.AppSettings["SMTPPort"]);
                if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SMTPUsername"]) &&
                    !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["SMTPPassword"]))
                {
                    smtpClient.Credentials =
                        new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUsername"],
                            ConfigurationManager.AppSettings["SMTPPassword"]);
                }
                smtpClient.EnableSsl = Boolean.Parse(ConfigurationManager.AppSettings["SMTPSSL"]);

                var mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.From = new MailAddress(ConfigurationManager.AppSettings["ServiceEmail"]);
                var sendTo = ConfigurationManager.AppSettings["SendTo"].Split(';').ToList();

                foreach (var email in sendTo)
                {
                    mail.To.Add(email);
                }

                mail.Subject = string.Format("Cycle Report - {0} {1}", shift, TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Timezone"])).ToShortDateString());

                if (File.Exists(reportPath))
                {
                    mail.Attachments.Add(new Attachment(reportPath));
                }

                var chartStream = ExcelService.ChartStream();
                chartStream.Seek(0, SeekOrigin.Begin);
                var chartAtt = new Attachment(chartStream, "chart.gif", System.Net.Mime.MediaTypeNames.Image.Gif) {ContentId = "chart"};
                mail.Attachments.Add(chartAtt);
                
                
                var bodyContent = ConfigurationManager.AppSettings["EmailHTML"];

                bodyContent = bodyContent.Replace(ReportDateKey, TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["Timezone"])).ToShortDateString());
                bodyContent = bodyContent.Replace(ShiftKey, shift.ToString());
                bodyContent = bodyContent.Replace(ReportSummaryKey, WebUtility.HtmlDecode(reportSummary));
                bodyContent = HttpUtility.UrlDecode(bodyContent);
                mail.Body = bodyContent;

                smtpClient.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                _ts.TraceEvent(TraceEventType.Error, 20, ex.ToString());
            }

            return false;
        }
    }
}
