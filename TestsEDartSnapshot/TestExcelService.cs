﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using EDartSnapshot;
using NUnit.Framework;
using System.Web;
using System.Windows.Forms.DataVisualization.Charting;
using DataPoint = EDartSnapshot.DataPoint;

namespace TestsEDartSnapshot
{
    [TestFixture]
    public class TestExcelService
    {
        [Test]
        public void CreateExcel()
        {
            var fileName = "Test.xlsx";

            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }

            var shift = Shift.Third;

            var html = ExcelService.CreateExcel(fileName, GetData(), shift);
            
            EmailService.SendEmail(fileName, shift, html);

        }

        private List<EDart> GetData()
        {
            var data = new List<EDart>();
            for (int i = 0; i < 5; i++)
            {
                var eDart = new EDart();
                eDart.Name = "eDart" + i;
                eDart.Tonnage = "Ton" + i;
                eDart.IP = "192.168.1.1"+i;
                eDart.Status = EDartStatus.Up;

                var r = new Random();
                int rInt = r.Next(180, 480);
                var cycle = r.Next(0, 1000);
                for (int j = 0; j < rInt; j++)
                {
                    var dataPoint = new DataPoint(DateTime.Now.AddMinutes(j));
                    dataPoint.Data = (float)19.96;
                    dataPoint.Type = DataType.Cycle;
                    dataPoint.ToolName = "tool1";// + r.Next(0,10);
                    dataPoint.Standard = 20.01;
                    dataPoint.Cycles = cycle + j;
                    eDart.DataPoints.Add(dataPoint);
                }

                for (int j = rInt; j < rInt+20; j++)
                {
                    var dataPoint = new DataPoint(DateTime.Now.AddMinutes(j));
                    dataPoint.Data = (float)19.96;
                    dataPoint.Type = DataType.Cycle;
                    dataPoint.ToolName = "tool1";// + r.Next(0,10);
                    dataPoint.Standard = 20.02;
                    dataPoint.Cycles = cycle + j;
                    eDart.DataPoints.Add(dataPoint);
                }

                for (int j = 0; j < r.Next(10,40); j++)
                {
                    var dataPoint = new DataPoint(DateTime.Now.AddMinutes(j));
                    dataPoint.Data = (float)21.5;
                    dataPoint.Type = DataType.Cycle;
                    dataPoint.ToolName = "tool2";// + r.Next(0,10);
                    dataPoint.Standard = 20.02;
                    dataPoint.Cycles = cycle + j;
                    eDart.DataPoints.Add(dataPoint);
                }
                for (int j = 0; j < r.Next(40,80); j++)
                {
                    var dataPoint = new DataPoint(DateTime.Now.AddMinutes(j));
                    dataPoint.Status = "Down";
                    dataPoint.Type = DataType.Downtime;
                    dataPoint.ToolName = "tool" + r.Next(0,10);
                    dataPoint.Standard = 20.01;
                    dataPoint.Cycles = cycle + j;
                    eDart.DataPoints.Add(dataPoint);
                }

                for (int j = 0; j < r.Next(40, 80); j++)
                {
                    var dataPoint = new DataPoint(DateTime.Now.AddMinutes(j));
                    dataPoint.Status = "No reason";
                    dataPoint.Type = DataType.Unknown;
                    dataPoint.ToolName = "tool" + r.Next(0, 10);
                    dataPoint.Standard = 20.01;
                    dataPoint.Cycles = cycle + j;
                    eDart.DataPoints.Add(dataPoint);
                }

                eDart.AddDataPoint(450.012, "tool2", 20.0, DataType.Cycle);


                data.Add(eDart);
            }

            return data;

        }

    }
}
